﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyImdb.Entities {
	public class MovieActor {
		public Guid Id { get; set; }
		[Required]
		[MaxLength(100)]
		public string Character { get; set; }
		[Required]
		public Guid MovieId { get; set; }
		public Movie Movie { get; set; }
		[Required]
		public Guid ActorId { get; set; }
		public Actor Actor { get; set; }
	}
}
