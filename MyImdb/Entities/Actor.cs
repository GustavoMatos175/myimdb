﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyImdb.Entities {
	public class Actor {
		public Guid Id { get; set; }
		[MaxLength(100)]
		[Required]
		public string Name { get; set; }
		[MaxLength(100)]
		[Required]
		public string Birthplace { get; set; }
		public List<MovieActor> MovieActors { get; set; }
	}

}
