﻿using Api.Actor;
using Api.Genre;
using Api.Movie;
using Api.MovieActor;
using MyImdb.Entities;

namespace MyImdb.Services {
	public class ModelConverter {
		public ModelConverter(
		) {
		}
		public GenreModel ToModel(Genre genre) {
			return new GenreModel() {
				Id = genre.Id,
				Name = genre.Name
			};
		}
		public MovieModel ToModel(Movie movie) {
			return new MovieModel() {
				Id = movie.Id,
				Title = movie.Title,
				Rank = movie.Rank,
				Year = movie.Year,
				Genre = ToModel(movie.Genre)
			};
		}
		public ActorModel ToModel(Actor actor) {
			return new ActorModel() {
				Id = actor.Id,
				Name = actor.Name,
				BirthPlace = actor.Birthplace
			};

		}
		public MovieActorModel ToModel(MovieActor movieActor) {
			return new MovieActorModel() {
				Id = movieActor.Id,
				Character = movieActor.Character,
				Movie = ToModel(movieActor.Movie),
				Actor = ToModel(movieActor.Actor)
			};
		}
	}
}

