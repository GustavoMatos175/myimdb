using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Configuration;
using MyImdb.Entities;
using MyImdb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb {
	public class Startup {
		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {
			services.AddControllersWithViews();
			services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
			services.AddScoped<MovieRepository>();
			services.AddScoped<MovieService>();
			services.AddScoped<ExceptionBuilder>();
			services.AddScoped<GenreRepository>();
			services.AddScoped<GenreService>();
			services.AddScoped<MovieActorRepository>();
			services.AddScoped<MovieActorService>();
			services.AddScoped<ActorRepository>();
			services.AddScoped<ActorService>();
			services.AddScoped<ModelConverter>();
			services.AddControllersWithViews(o => {
				o.Filters.Add<HandleExceptionFilter>();
				o.Filters.Add<ValidateModelStateAttribute>();
			}).AddNewtonsoftJson();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			} else {
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}
			app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints => {
				//endpoints.MapControllerRoute(
				//	name: "default",
				//	pattern: "{controller=Home}/{action=Index}/{id?}");
				endpoints.MapControllers();
			});
		}
	}
}
