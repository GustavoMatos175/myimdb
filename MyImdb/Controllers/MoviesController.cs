﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyImdb.Models;
using MyImdb.ViewModels;
using MyImdb.Entities;
using Api.Movie;
using MyImdb.Business.Repositories;
using MyImdb.Services;
using MyImdb.Business;
using Api.MovieActor;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/movies")]
	public class MoviesController : Controller {
		private readonly MovieRepository movieRepository;
		private readonly ModelConverter mc;
		private readonly MovieService movieService;
		private readonly GenreRepository genreRepository;
		private readonly MovieActorService movieActorService;
		private readonly ActorRepository actorRepository;

		public MoviesController(MovieRepository movieRepository, ModelConverter mc, MovieService movieService, 
			GenreRepository genreRepository, MovieActorService movieActorService, ActorRepository actorRepository) {
			this.movieRepository = movieRepository;
			this.mc = mc;
			this.movieService = movieService;
			this.genreRepository = genreRepository;
			this.movieActorService = movieActorService;
			this.actorRepository = actorRepository;
		}

		public IActionResult Index(string msg = null) {
			//var movies = Movie.SelectAll().ConvertAll(m => new MovieListViewModel() {
			//	Rank = m.Rank,
			//	Title = m.Title,
			//	Year = m.Year
			//});
			//ViewBag.Message = msg;
			//return View(movies);
			return View();
		}

		public IActionResult MovieOfTheMonth() {
			//var movieOfTheMonth = Movie.SelectAll().OrderBy(r => r.Rank).Take(1).ToList();
			//return View(movieOfTheMonth);
			return View();
		}

		public IActionResult Create() {
			return View();
		}

		//[HttpPost]
		//public IActionResult Create(MovieCreateViewModel model) {
		//	if (!ModelState.IsValid) {
		//		return View(model);
		//	}
		//	return RedirectToAction(nameof(Index), new { msg = "Movie created with success." });
		//}
		[HttpPost]
		public async Task<MovieModel> Create(MovieData request) {
			var genre = await genreRepository.SelectByNameAsync(request.Genre.Name);
			var movie = await movieService.CreateAsync(request.Rank, request.Title, request.Year, request.StoryLine, genre.Id);
			return mc.ToModel(movie);
		}
		//Get list of movies
		[HttpGet]
		public async Task<List<MovieModel>> List(int n = 20) {
			var movies = await movieRepository.SelectTopNAsync(n);
			return movies.ConvertAll(g => mc.ToModel(g));
		}
		//Get movie
		[HttpGet("{id}")]
		public async Task<MovieModel> Get(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);
			return mc.ToModel(movie);
		}
		//Update movie title
		[HttpPut("{id}")]
		public async Task<MovieModel> Update(Guid id, MovieUpdateTitleRequest request) {
			var movie = await movieRepository.SelectByIdAsync(id);
			await movieService.UpdateTitleAsync(movie, request.Title);
			return mc.ToModel(movie);
		}
		//Delete movie
		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);
			await movieService.DeleteAsync(movie);
		}
		//Create movieactor
		[HttpPost("{id}/actors")]
		public async Task<MovieActorModel> Create(Guid id, MovieActorData request) {
			var movie = await movieRepository.SelectByIdAsync(id);
			var actor = await actorRepository.SelectByIdAsync(request.Actor.Id);
			var movieActor = await movieActorService.CreateAsync(request.Character, request.Actor.Id, movie.Id);
			movieActor.Movie =  movie;
			movieActor.Actor =  actor;
			return mc.ToModel(movieActor);
		}



		
	}
}
