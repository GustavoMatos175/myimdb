﻿using Microsoft.AspNetCore.Mvc;
using Api.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyImdb.Business.Repositories;
using MyImdb.Services;
using MyImdb.Business;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/actors")]
	public class ActorsController {
		private readonly ActorRepository actorRepository;
		private readonly ModelConverter mc;
		private readonly ActorService actorService;

		public ActorsController(ActorRepository actorRepository, ModelConverter mc, ActorService actorService) {
			this.actorRepository = actorRepository;
			this.mc = mc;
			this.actorService = actorService;
		}

		[HttpGet]
		public async Task<List<ActorModel>> List(int n = 10) {
			var actors = await actorRepository.SelectTopNAsync(n);
			return actors.ConvertAll(a => mc.ToModel(a));
		}


		[HttpPost]
		public async Task<ActorModel> Create(ActorData request) {
			var actor = await actorService.CreateAsync(request.Name, request.BirthPlace);
			return mc.ToModel(actor);
		}

		[HttpGet("{id}")]
		public async Task<ActorModel> Get(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);
			return mc.ToModel(actor);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var actor = await actorRepository.SelectByIdAsync(id);
			await actorService.DeleteAsync(actor);
		}
		[HttpPut("{id}")]
		public async Task<ActorModel> Update(Guid id, ActorData request) {
			var actor = await actorRepository.SelectByIdAsync(id);
			await actorService.UpdateNameAsync(actor, request.Name);
			return mc.ToModel(actor);
		}
	}
}
