﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyImdb.Entities;
using MyImdb.Models;
using MyImdb.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Controllers {
	public class HomeController : Controller {
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger) {
			_logger = logger;
		}

		public IActionResult Index() {
			//var moviesOfTheMonth = Movie.SelectAll().OrderBy(r => r.Rank).Take(3).ToList().ConvertAll(m => new MovieListViewModel() { Rank = m.Rank, Title = m.Title, Year = m.Year });
			//return View(moviesOfTheMonth);
			return View();
		}

		public IActionResult Privacy() {
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error() {
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
