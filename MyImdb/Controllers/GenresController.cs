﻿using Api.Genre;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Controllers {
	[ApiController]
	[Route("api/genres")]
	public class GenresController : Controller {
		private readonly GenreRepository genreRepository;
		private readonly ModelConverter mc;
		private readonly GenreService genreService;

		public GenresController(GenreRepository genreRepository, ModelConverter mc, GenreService genreService) {
			this.genreRepository = genreRepository;
			this.mc = mc;
			this.genreService = genreService;
		}
		//Get list of genres
		[HttpGet]
		public async Task<List<GenreModel>> List(int n = 20) {
			var genres = await genreRepository.SelectTopNAsync(n);
			throw new Exception("An error occured in http get genreController");
			return genres.ConvertAll(g => mc.ToModel(g));
		}
		//Get genre
		[HttpGet("{id}")]
		public async Task<GenreModel> Get(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);
			return mc.ToModel(genre);
		}
		//Create genre
		[HttpPost]
		public async Task<GenreModel> Create(GenreData request) {
			var genre = await genreService.CreateAsync(request.Name);
			return mc.ToModel(genre);
		}
		//Update genre name
		[HttpPut("{id}")]
		public async Task<GenreModel> Update(Guid id, GenreData request) {
			var genre = await genreRepository.SelectByIdAsync(id);
			await genreService.UpdateAsync(genre, request.Name);
			return mc.ToModel(genre);
		}
		//Delete Genre
		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);
			await genreService.DeleteAsync(genre);
		}

	}
}
