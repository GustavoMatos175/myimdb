﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business.Repositories {
	public class GenreRepository {

		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;
		public GenreRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<List<Genre>> SelectTopNAsync(int n = 20) {
			var query = dbContext.Genres.OrderBy(g => g.Name).AsQueryable();
			query = query.Take(n);
			return await query.ToListAsync();
		}

		public async Task<Genre> CreateAsync(string name) {
			var genre = new Genre() {
				Id = Guid.NewGuid(),
				Name = name
			};
			await dbContext.AddAsync(genre);
			return genre;
		}
		public async Task<Genre> SelectByNameAsync(string name) {
			return await dbContext.Genres.FirstOrDefaultAsync(g => g.Name == name);
		}
		public async Task<Genre> SelectByIdAsync(Guid id) {
			return await dbContext.Genres.FirstOrDefaultAsync(g => g.Id == id) ?? 
				throw exceptionBuilder.Api(Api.ErrorCodes.GenreNotFound, new { id });
		}
	}
}
