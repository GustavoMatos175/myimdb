﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business.Repositories {
	public class MovieActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;


		public MovieActorRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		//public async Task<List<MovieActor>> SelectTopNAsync(int n = 20) {
		//	var query = dbContext.MovieActors.OrderBy(m => m.Actor).AsQueryable();
		//	query = query.Take(n);
		//	return await query.ToListAsync();
		//}
		public async Task<MovieActor> CreateAsync(Guid actorId, Guid movieId, string character) {
			var movieActor = new MovieActor() {
				Id = Guid.NewGuid(),
				ActorId = actorId,
				Character = character,
				MovieId = movieId

			};
			await dbContext.AddAsync(movieActor);
			return movieActor;
		}
		public async Task<MovieActor> SelectByCharacterAsync(string character, Guid movieId) {
			return await dbContext.MovieActors.FirstOrDefaultAsync(m => m.Character == character && m.MovieId == movieId)
			??  throw exceptionBuilder.Api(Api.ErrorCodes.CharacterNotFound, new { character});
		}
		public async Task<List<MovieActor>> SelectByMovieIdAsync(Guid id) {
			return await dbContext.MovieActors.Where(m => m.MovieId == id).ToListAsync();
		}

	}
}
