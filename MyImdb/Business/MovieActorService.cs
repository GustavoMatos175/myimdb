﻿using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business {
	public class MovieActorService {
		private readonly MovieActorRepository movieActorRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;
		private readonly ActorRepository actorRepository;
		private readonly MovieRepository movieRepository;

		public MovieActorService(MovieActorRepository movieActorRepository, ExceptionBuilder exceptionBuilder, AppDbContext dbContext
			,ActorRepository actorRepository, MovieRepository movieRepository) {
			this.movieActorRepository = movieActorRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
			this.actorRepository = actorRepository;
			this.movieRepository = movieRepository;
		}

		public async Task<MovieActor> CreateAsync(string character, Guid actorId, Guid movieId) {
			var movie = await movieRepository.SelectByIdAsync(movieId);
			var existingMovieActor = await movieActorRepository.SelectByCharacterAsync(character, movieId);
			if (existingMovieActor != null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.CharacterAlreadyExists, new { character, movie.Title });
			}
			var actor = await actorRepository.SelectByIdAsync(actorId);
			
			var movieActor = await movieActorRepository.CreateAsync(actor.Id, movie.Id, character);
			await dbContext.SaveChangesAsync();
			return movieActor;
		}

	}
}
