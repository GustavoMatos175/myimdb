﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business {
	public class MovieService {
		private readonly MovieRepository movieRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;
		private readonly GenreRepository genreRepository;
		private readonly MovieActorRepository movieActorRepository;

		public MovieService(MovieRepository movieRepository, ExceptionBuilder exceptionBuilder, 
			AppDbContext dbContext, GenreRepository genreRepository, MovieActorRepository movieActorRepository) {
			this.movieRepository = movieRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
			this.genreRepository = genreRepository;
			this.movieActorRepository = movieActorRepository;
		}
		public async Task<Movie> CreateAsync(int rank, string title, int year, string storyline, Guid genreid) {
			var existingMovie = await movieRepository.SelectByTitleAsync(title);
			if (existingMovie != null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieTitleAlreadyExists, new { title });
			}

			var genre = await genreRepository.SelectByIdAsync(genreid);

			var movie = await movieRepository.CreateAsync(rank, title, year, storyline, genre.Id);
			await dbContext.SaveChangesAsync();
			return movie;
		}
		public async Task UpdateTitleAsync(Movie movie, string title) {
			if (await dbContext.Movies.AnyAsync(m => m.Title == title && m.Id != movie.Id)) {
				throw exceptionBuilder.Api(Api.ErrorCodes.MovieTitleAlreadyExists);
			}
			movie.Title = title;
			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(Movie movie) {
			var movieActors = await movieActorRepository.SelectByMovieIdAsync(movie.Id);
			//Delete all MovieActors
			dbContext.RemoveRange(movieActors);
			dbContext.Remove(movie);
			await dbContext.SaveChangesAsync();
		}
	}
}
