﻿using Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business {
	public class ExceptionBuilder {
		public ApiException Api(ErrorCodes code, object details = null) {
			var message = string.Empty;
			switch (code) {
				case ErrorCodes.Unknown:
					message = "An Unknown error has occurred";
					break;
				case ErrorCodes.MovieTitleAlreadyExists:
					message = "A movie with this title already exists";
					break;
				case ErrorCodes.ActorAlreadyExists:
					message = "An actor already exists";
					break;
				case ErrorCodes.ActorNotFound:
					message = "Actor not fount";
					break;
				case ErrorCodes.CharacterAlreadyExists:
					message = "Character already exists";
					break;
				case ErrorCodes.CharacterNotFound:
					message = "Charact not found";
					break;
				case ErrorCodes.MovieNotFound:
					message = "Movie not found";
					break;
				default:
					break;
			}
			return new ApiException(new ErrorModel() {
				Code = code,
				Message = message,
				Details = getDetailsDictionary(details)
			});
		}
		private static Dictionary<string, string> getDetailsDictionary(object details) {
			var dic = new Dictionary<string, string>();
			foreach (var descriptor in details.GetType().GetProperties()) {
				dic[descriptor.Name] = descriptor.GetValue(details,
				null)?.ToString() ?? "null";
			}
			return dic;
		}
	}
}
