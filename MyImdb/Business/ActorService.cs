﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business {
	public class ActorService {
		private readonly ActorRepository actorRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;

		public ActorService(ActorRepository actorRepository, ExceptionBuilder exceptionBuilder, AppDbContext dbContext) {
			this.actorRepository = actorRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
		}

		public async Task<Actor> CreateAsync(string name, string birthplace) {
			var existingActor = await actorRepository.SelectByNameAsync(name);
			if (existingActor != null) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorAlreadyExists, new { name });
			}

			var actor = await actorRepository.CreateAsync(name, birthplace);
			await dbContext.SaveChangesAsync();
			return actor;
		}

		public async Task UpdateNameAsync(Actor actor, string name) {
			if (await dbContext.Actors.AnyAsync(a => a.Name == name && a.Id != actor.Id)) {
				throw exceptionBuilder.Api(Api.ErrorCodes.ActorAlreadyExists, new { name });
			}
			actor.Name = name;
			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(Actor actor) {
			//Remover id do MovieActor quando terminar o movieactorcontroller
			dbContext.Remove(actor);
			await dbContext.SaveChangesAsync();
		}

		

	}
}
