﻿using Api.Actor;
using Api.Movie;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.MovieActor {
	public class MovieActorData {
		public Guid Id { get; set; }
		public string Character { get; set; }
		public MovieModel Movie { get; set; }
		public ActorModel Actor { get; set; }
	}
}
