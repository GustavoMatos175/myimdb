﻿using System;
using Api.Genre;

namespace Api.Movie {
	public class MovieModel {
		public Guid Id { get; set; }
		public int Rank { get; set; }
		public string Title { get; set; }
		public int Year { get; set; }
		public GenreModel Genre { get; set; }
	}
}
