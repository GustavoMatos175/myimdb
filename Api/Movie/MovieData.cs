﻿using Api.Genre;
using Api.MovieActor;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.Movie {
	public class MovieData {
		public int Rank { get; set; }
		[Required]
		public string Title { get; set; }
		public int Year { get; set; }
		public string StoryLine { get; set; }
		public GenreModel Genre { get; set; }
	}
	public class MovieUpdateTitleRequest {
		[Required]
		public string Title { get; set; }
	}
}
