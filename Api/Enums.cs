﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api
{
    public enum ErrorCodes
    {
		Unknown = 0,
		MovieTitleAlreadyExists,
		GenreNotFound,
		CharacterNotFound,
		ActorNotFound,
		CharacterAlreadyExists,
		ActorAlreadyExists,
		MovieNotFound,
		GenreNameAlreadyExists,
	}
}
