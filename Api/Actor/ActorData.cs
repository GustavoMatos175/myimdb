﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.Actor {
	public class ActorData {
		[Required(ErrorMessage = "The name of the Actor is required")]
		[MaxLength(100,ErrorMessage = "The name can't be greater than {1} characters")]
		public string Name { get; set; }
		[Required(ErrorMessage = "The name of the bithplace is required")]
		[MaxLength(100, ErrorMessage = "The name can't be greater than {1} characters")]
		public string BirthPlace { get; set; }

	}
}
